package dev.omargt.springboot.demo.healthybody.model;

import org.springframework.stereotype.Component;

@Component
public class TrackCoach extends CoachInformation implements Coach {

    @Override
    public String getDailyWorkout() {
        return "Run a hard 10k!";
    }

}
