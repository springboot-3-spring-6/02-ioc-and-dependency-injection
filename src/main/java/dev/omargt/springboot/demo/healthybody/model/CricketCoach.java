package dev.omargt.springboot.demo.healthybody.model;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.stereotype.Component;

@Component
public class CricketCoach extends CoachInformation implements Coach {

    // After construct cricket, do this
    @PostConstruct
    public void postConstruct(){
        System.out.println("Cricket was constructed!");
    }

    // Before destroy cricket (like end app, or killing object) , do this
    @PreDestroy
    public void preDestroy(){
        System.out.println("Destroying cricket...");
    }

    @Override
    public String getDailyWorkout() {
        return "Practice fast blowing for 15 minutes. Yes! x2";
    }

}
