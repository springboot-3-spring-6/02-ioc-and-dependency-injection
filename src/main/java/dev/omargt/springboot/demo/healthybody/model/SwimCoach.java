package dev.omargt.springboot.demo.healthybody.model;

// Configuring this in startup as a component.
// @Component annotation does not necessary.
public class SwimCoach extends CoachInformation implements Coach {

    @Override
    public String getDailyWorkout() {
        return "Do 5 laps in the pool";
    }

}
