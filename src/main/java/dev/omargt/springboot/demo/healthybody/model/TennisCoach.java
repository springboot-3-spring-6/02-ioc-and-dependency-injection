package dev.omargt.springboot.demo.healthybody.model;

import org.springframework.stereotype.Component;

@Component
public class TennisCoach extends CoachInformation implements Coach{

    @Override
    public String getDailyWorkout() {
        return "Practice your backhand volley";
    }

}
