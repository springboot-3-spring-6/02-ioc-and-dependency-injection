package dev.omargt.springboot.demo.healthybody.rest;

import dev.omargt.springboot.demo.healthybody.model.Coach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@Lazy // Not recommended here, because a rest api always (well, often) used by someone
public class DemoController {

    private Coach coach;

    // Dependency Injection
    // In this case, we have only 1 coach so, Spring figure this out
    // selecting the only "Component" that implements Coach
    @Autowired
    public DemoController(@Qualifier("mySwim") Coach coach){
        System.out.println("Instance: " + getClass().getSimpleName());
        this.coach = coach;
    }

    @GetMapping("/dailyworkout")
    public String getDailyWorkout(){
       return coach.getDailyWorkout();
    }

}
