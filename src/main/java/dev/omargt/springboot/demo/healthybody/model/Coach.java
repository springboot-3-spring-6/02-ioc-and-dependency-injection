package dev.omargt.springboot.demo.healthybody.model;

public interface Coach {

    String getDailyWorkout();

}
