package dev.omargt.springboot.demo.healthybody.config;

import dev.omargt.springboot.demo.healthybody.model.Coach;
import dev.omargt.springboot.demo.healthybody.model.SwimCoach;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SportConfig {

    // We are setting a swim coach as a bean to use in the app.
    // For default, the id of the bean it would be the method name, unless
    // you set it. @Bean("idName")
    @Bean("mySwim")
    public Coach swimCoach(){
        return new SwimCoach();
    }

}
