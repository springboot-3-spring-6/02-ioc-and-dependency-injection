package dev.omargt.springboot.demo.healthybody;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(
/*
                            // or simply dev.omargt.springboot.demo
        scanBasePackages = {"dev.omargt.springboot.demo.healthybody",
                            "dev.omargt.springboot.demo.util"}
*/
)
public class HealthyBodyApplication {

    public static void main(String[] args) {
        SpringApplication.run(HealthyBodyApplication.class, args);
    }

}
