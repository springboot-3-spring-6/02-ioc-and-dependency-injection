# Healthy Body App with Spring (#02)
This app is for apply concepts for Spring Boot 3 and Spring 6 from
this [udemy course](https://www.udemy.com/course/spring-hibernate-tutorial/).

> From the mentioned course, this would be the second section, you can see
> other sections in this group repo in [gitlab](https://gitlab.com/springboot-3-spring-6)

## General information
In this repo, is covering how spring works and features that provides us
like IoC concept, depdency injection, etc. Each commit has an specific feature/concept added.

## Concepts learned
- IoC - Inversion of Control (How does Spring store objects (components/beans)
in our application.
- Dependency Injection (With @Autowired annotation to auto injection our
components).
- Read components/beans configuration (Set packages to read with @SpringBootApplication)
- Setter Injection (With @Autowired in some "setter" method). Use when the object
injected will be optional.
- Qualifier annotation - If we have two or more types of beans, we can define
which we will use, e.g: Two Coaches implements Coach.
- Primary (With @Primary annotation) - We can define an "default" annotation 
to set when we have two or more types of beans. @Qualifier has more priority.
- Lazy beans - Define beans as lazy for not auto initialization (Only when 
each one of them are required by something), it can be global config or for
each one.
- Bean Scope - For default, all beans are initialized with singleton "mode", it
means, each bean is only created once in the app. You can change this.
- Bean Lifecycle - Beans with singleton scope have @PostConstruct and @PreDestroy
methods. Beans with prototype cycle only have @PostConstruct method.
- Configuration - We can configure our spring application, adding beans to
use third party library classes especially.

## Requirements
- You need at least Java 17 or higher.

## Extra information
For this project, I'm using Intellij IDEA.
